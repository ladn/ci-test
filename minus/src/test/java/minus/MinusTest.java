package minus;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

public class MinusTest {

    @Test
    public void plusTest() {
        assertThat(
                App.minus(2,1),
                equalTo(1)
        );
    }

}

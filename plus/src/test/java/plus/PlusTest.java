package plus;


import org.junit.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

public class PlusTest {

    @Test
    public void plusTest1() {
        assertThat(
                App.plus(1,1),
                equalTo(2)
        );
    }

    @Test
    public void plusTest2() {
        assertThat(
                App.plus(2,2),
                equalTo(4)
        );
    }

    @Test
    public void plusTest3() {
        assertThat(
                App.plus(20,20),
                equalTo(40)
        );
    }

}
